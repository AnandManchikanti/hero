import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('testing the Usercomponent title',()=>{
    expect(component.ComponentName).toBe("user");
  });

  it("Testing function",()=>{
    expect(component.sum(40,60)).toBe(100)
  })

  it("Testing Html Element",()=>{
    const data=fixture.nativeElement;
    expect(data.querySelector(".some").textContent).toContain("user is working")
  })

});
